from django.db import models


class Cashbook(models.Model):
    amount = models.FloatField()
    # TODO Replace "shop" by "category"
    shop = models.CharField(max_length=100)
    date = models.DateField(null=True, default=None)
